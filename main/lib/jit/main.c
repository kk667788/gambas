/***************************************************************************

	main.c

	(c) 2000-2018 Benoît Minisini <benoit.minisini@gambas-basic.org>

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2, or (at your option)
	any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	MA 02110-1301, USA.

***************************************************************************/

#define __MAIN_C

#include "jit.h"
#include "main.h"

GB_INTERFACE *GB_PTR EXPORT;
JIT_INTERFACE *JIT_PTR EXPORT;

static char *_translation = NULL;
static char *_pointers = NULL;


BEGIN_METHOD(Jit_Translate, GB_STRING klass; GB_STRING from; GB_BOOLEAN trace; GB_BOOLEAN cache)

	GB.FreeString(&_translation);
	GB.FreeString(&_pointers);

	GB.ReturnBoolean(JIT_translate(GB.ToZeroString(ARG(klass)), GB.ToZeroString(ARG(from)), VARG(trace), VARG(cache), &_translation, &_pointers));

END_METHOD

BEGIN_PROPERTY(Jit_Translation)

	GB.ReturnString(_translation);

END_PROPERTY

BEGIN_PROPERTY(Jit_Pointers)

	GB.ReturnString(_pointers);

END_PROPERTY

BEGIN_METHOD_VOID(Jit_exit)

	GB.FreeString(&_translation);
	GB.FreeString(&_pointers);

END_METHOD

GB_DESC JitDesc[] =
{
	GB_DECLARE_STATIC("__Jit"),

	GB_STATIC_METHOD("Translate", "b", Jit_Translate, "(Class)s(From)s(Trace)b(Cache)b"),
	GB_STATIC_PROPERTY_READ("Translation", "s", Jit_Translation),
	GB_STATIC_PROPERTY_READ("Pointers", "s", Jit_Pointers),

	GB_STATIC_METHOD("_exit", NULL, Jit_exit, NULL),

	GB_END_DECLARE
};


GB_DESC *GB_CLASSES [] EXPORT =
{
	JitDesc,
	NULL
};


int EXPORT GB_INIT(void)
{
	return 0;
}


void EXPORT GB_EXIT()
{
}

