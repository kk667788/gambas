#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.util 3.16.90\n"
"POT-Creation-Date: 2022-02-04 01:26 UTC\n"
"PO-Revision-Date: 2022-02-04 01:30 UTC\n"
"Last-Translator: Benoît Minisini <g4mba5@gmail.com>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Utility routines"
msgstr ""

#: File.class:22
msgid "&1 B"
msgstr ""

#: File.class:24
msgid "&1 KiB"
msgstr ""

#: File.class:26
msgid "&1 MiB"
msgstr ""

#: File.class:28
msgid "&1 GiB"
msgstr ""

#: File.class:34
msgid "&1 KB"
msgstr ""

#: File.class:36
msgid "&1 MB"
msgstr ""

#: File.class:38
msgid "&1 GB"
msgstr ""

#: Language.class:14
msgid "Afrikaans (South Africa)"
msgstr ""

#: Language.class:17
msgid "Arabic (Egypt)"
msgstr "アラビア語 (エジプト)"

#: Language.class:18
msgid "Arabic (Tunisia)"
msgstr "アラビア語 (チュニジア)"

#: Language.class:21
msgid "Azerbaijani (Azerbaijan)"
msgstr "アゼルバイジャン語(アゼルバイジャン)"

#: Language.class:24
msgid "Bulgarian (Bulgaria)"
msgstr "ブルガリア (ブルガリア語)"

#: Language.class:27
msgid "Catalan (Catalonia, Spain)"
msgstr "カタロニア語 (カタロニア、スペイン)"

#: Language.class:31
msgid "Welsh (United Kingdom)"
msgstr "ウェールズ (イギリス)"

#: Language.class:34
msgid "Czech (Czech Republic)"
msgstr "チェコ語 (チェコ共和国)"

#: Language.class:37
msgid "Danish (Denmark)"
msgstr "デンマーク語 (デンマーク)"

#: Language.class:40
msgid "German (Germany)"
msgstr "ドイツ語 (ドイツ)"

#: Language.class:41
msgid "German (Belgium)"
msgstr "ドイツ語 (ベルギー)"

#: Language.class:44
msgid "Greek (Greece)"
msgstr "ギリシャ語 (ギリシャ)"

#: Language.class:47
msgid "English (common)"
msgstr "英語 (共通)"

#: Language.class:48
msgid "English (United Kingdom)"
msgstr "英語 (イギリス)"

#: Language.class:49
msgid "English (U.S.A.)"
msgstr "英語 (アメリカ)"

#: Language.class:50
msgid "English (Australia)"
msgstr "英語 (オーストラリア)"

#: Language.class:51
msgid "English (Canada)"
msgstr "英語 (カナダ)"

#: Language.class:54
msgid "Esperanto (Anywhere!)"
msgstr "エスペラント語(どこでも!）"

#: Language.class:57
msgid "Spanish (common)"
msgstr ""

#: Language.class:58
msgid "Spanish (Spain)"
msgstr "スペイン語 (スペイン)"

#: Language.class:59
msgid "Spanish (Argentina)"
msgstr "スペイン語 (アルゼンチン)"

#: Language.class:62
msgid "Estonian (Estonia)"
msgstr "エストニア語 (エストニア)"

#: Language.class:65
msgid "Basque (Basque country)"
msgstr ""

#: Language.class:68
msgid "Farsi (Iran)"
msgstr "近代ペルシャ語(イラン)"

#: Language.class:71
msgid "Finnish (Finland)"
msgstr ""

#: Language.class:74
msgid "French (France)"
msgstr "フランス語 (フランス)"

#: Language.class:75
msgid "French (Belgium)"
msgstr "フランス語 (ベルギー)"

#: Language.class:76
msgid "French (Canada)"
msgstr "フランス語 (カナダ)"

#: Language.class:77
msgid "French (Switzerland)"
msgstr "フランス語 (スイス)"

#: Language.class:80
msgid "Galician (Spain)"
msgstr "ガリシア語 (スペイン)"

#: Language.class:83
msgid "Hebrew (Israel)"
msgstr "ヘブライ語 (イスラエル)"

#: Language.class:86
msgid "Hindi (India)"
msgstr "ヒンディー語 (インド)"

#: Language.class:89
msgid "Hungarian (Hungary)"
msgstr "ハンガリー語 (ハンガリー)"

#: Language.class:92
msgid "Croatian (Croatia)"
msgstr "クロアチア (クロアチア語)"

#: Language.class:95
msgid "Indonesian (Indonesia)"
msgstr "インドネシア語 (インドネシア)"

#: Language.class:98
msgid "Irish (Ireland)"
msgstr "アイルランド語 (アイルランド)"

#: Language.class:101
msgid "Icelandic (Iceland)"
msgstr ""

#: Language.class:104
msgid "Italian (Italy)"
msgstr "イタリア語 (イタリア)"

#: Language.class:107
msgid "Japanese (Japan)"
msgstr "日本語 (日本)"

#: Language.class:110
msgid "Khmer (Cambodia)"
msgstr "クメール語 (カンボジア)"

#: Language.class:113
msgid "Korean (Korea)"
msgstr "韓国語(韓国)"

#: Language.class:116
msgid "Latin"
msgstr ""

#: Language.class:119
msgid "Lithuanian (Lithuania)"
msgstr ""

#: Language.class:122
msgid "Malayalam (India)"
msgstr "マラヤーラム語 (インド)"

#: Language.class:125
msgid "Macedonian (Republic of Macedonia)"
msgstr ""

#: Language.class:128
msgid "Dutch (Netherlands)"
msgstr "オランダ語 (オランダ)"

#: Language.class:129
msgid "Dutch (Belgium)"
msgstr "オランダ語 (ベルギー)"

#: Language.class:132
msgid "Norwegian (Norway)"
msgstr "ノルウェー語 (ノルウェー)"

#: Language.class:135
msgid "Punjabi (India)"
msgstr "パンジャブ語(インド)"

#: Language.class:138
msgid "Polish (Poland)"
msgstr "ポーランド語 (ポーランド)"

#: Language.class:141
msgid "Portuguese (Portugal)"
msgstr "ポルトガル語 (ポルトガル)"

#: Language.class:142
msgid "Portuguese (Brazil)"
msgstr "ポルトガル語 (ブラジル)"

#: Language.class:145
msgid "Valencian (Valencian Community, Spain)"
msgstr ""

#: Language.class:148
msgid "Romanian (Romania)"
msgstr ""

#: Language.class:151
msgid "Russian (Russia)"
msgstr "ロシア語 (ロシア)"

#: Language.class:154
msgid "Slovenian (Slovenia)"
msgstr "スロベニア語 (スロベニア)"

#: Language.class:157
msgid "Albanian (Albania)"
msgstr "アルバニア語 (アルバニア)"

#: Language.class:160
msgid "Serbian (Serbia & Montenegro)"
msgstr "セルビア語(セルビア・モンテネグロ)"

#: Language.class:163
msgid "Swedish (Sweden)"
msgstr "スウェーデン語 (スウェーデン)"

#: Language.class:166
msgid "Turkish (Turkey)"
msgstr "トルコ語 (トルコ)"

#: Language.class:169
msgid "Ukrainian (Ukrain)"
msgstr ""

#: Language.class:172
msgid "Vietnamese (Vietnam)"
msgstr "ベトナム語(ベトナム)"

#: Language.class:175
msgid "Wallon (Belgium)"
msgstr "ワロン語 (ベルギー)"

#: Language.class:178
msgid "Simplified chinese (China)"
msgstr "簡体字中国語 (中国)"

#: Language.class:179
msgid "Traditional chinese (Taiwan)"
msgstr "繁体字中国語 (台湾)"

#: Language.class:241
msgid "Unknown"
msgstr "不明"
