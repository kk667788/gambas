# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
# Translators:
# Дмитрий Ошкало <dmitry.oshkalo@gmail.com>, 2019
# Kашицин Роман <calorus@gmail.com>, 2019
# Олег o1hk <o1h2k3@yandex.ru>, 2019
# AlexL <loginov.alex.valer@gmail.com>, 2019
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-28 09:00+0300\n"
"PO-Revision-Date: 2019-05-09 00:48+0000\n"
"Last-Translator: AlexL <loginov.alex.valer@gmail.com>, 2019\n"
"Language-Team: Russian (https://www.transifex.com/rus-open-source/teams/44267/ru/)\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: app/examples/Misc/Notepad/.project:21
msgid "A little text editor"
msgstr "Небольшой текстовый редактор"

#: app/examples/Misc/Notepad/.project:22 app/examples/Misc/Notepad/.src/FAbout.form:20
msgid "This is a little notepad sample program."
msgstr "Это небольшой пример программы-блокнота."

#: app/examples/Misc/Notepad/.src/FAbout.form:5
msgid "About..."
msgstr "О программе..."

#: app/examples/Misc/Notepad/.src/FAbout.form:14
msgid "OK"
msgstr "ОК"

#: app/examples/Misc/Notepad/.src/FNotepad.class:27
msgid "(New document)"
msgstr "(Новый документ)"

#: app/examples/Misc/Notepad/.src/FNotepad.class:63
msgid ""
"\n"
"\n"
"File has been modified. Do you want to save it ?"
msgstr ""
"\n"
"\n"
"Файл был изменён. Вы хотите сохранить его?"

#: app/examples/Misc/Notepad/.src/FNotepad.class:63
msgid "Yes"
msgstr "Да"

#: app/examples/Misc/Notepad/.src/FNotepad.class:63
msgid "No"
msgstr "Нет"

#: app/examples/Misc/Notepad/.src/FNotepad.class:63
msgid "Cancel"
msgstr "Отмена"

#: app/examples/Misc/Notepad/.src/FNotepad.class:94
msgid ""
"\n"
"Unable to load file.\n"
msgstr ""
"\n"
"Невозможно загрузить файл.\n"

#: app/examples/Misc/Notepad/.src/FNotepad.class:115
msgid "All files"
msgstr "Все файлы"

#: app/examples/Misc/Notepad/.src/FNotepad.class:115
msgid "C/C++ files"
msgstr "Файлы C/C++"

#: app/examples/Misc/Notepad/.src/FNotepad.class:115
msgid "Text files"
msgstr "Текстовые файлы"

#: app/examples/Misc/Notepad/.src/FNotepad.class:115
msgid "Desktop files"
msgstr "Файлы Рабочего стола"

#: app/examples/Misc/Notepad/.src/FNotepad.form:5
msgid "Little notepad"
msgstr "Маленький блокнот"

#: app/examples/Misc/Notepad/.src/FNotepad.form:9
msgid "File"
msgstr "Файл"

#: app/examples/Misc/Notepad/.src/FNotepad.form:11
msgid "Open"
msgstr "Открыть"

#: app/examples/Misc/Notepad/.src/FNotepad.form:16
msgid "Close"
msgstr "Закрыть"

#: app/examples/Misc/Notepad/.src/FNotepad.form:23
msgid "Save"
msgstr "Сохранить"

#: app/examples/Misc/Notepad/.src/FNotepad.form:28
msgid "Save As"
msgstr "Сохранить как"

#: app/examples/Misc/Notepad/.src/FNotepad.form:34
msgid "Quit"
msgstr "Выход"

#: app/examples/Misc/Notepad/.src/FNotepad.form:40
msgid "Edit"
msgstr "Редактировать"

#: app/examples/Misc/Notepad/.src/FNotepad.form:42
msgid "Copy"
msgstr "Копировать"

#: app/examples/Misc/Notepad/.src/FNotepad.form:47
msgid "Cut"
msgstr "Вырезать"

#: app/examples/Misc/Notepad/.src/FNotepad.form:52
msgid "Paste"
msgstr "Вставить"

#: app/examples/Misc/Notepad/.src/FNotepad.form:59
msgid "Undo"
msgstr "Откатить"

#: app/examples/Misc/Notepad/.src/FNotepad.form:64
msgid "Redo"
msgstr "Вернуть"

#: app/examples/Misc/Notepad/.src/FNotepad.form:71
msgid "Choose Font"
msgstr "Выбор шрифта"

#: app/examples/Misc/Notepad/.src/FNotepad.form:74
msgid "Wrap text"
msgstr "Переносить текст"

#: app/examples/Misc/Notepad/.src/FNotepad.form:80
msgid "?"
msgstr "?"

#: app/examples/Misc/Notepad/.src/FNotepad.form:82
msgid "About"
msgstr "О программе"

#: app/examples/Misc/Notepad/.src/FNotepad.form:89
msgid "txtNotepad"
msgstr "Текстовый блокнот"

