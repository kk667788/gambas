' Gambas class file

Export

Class Stock

' Static Private $aDataDir As String[]
Static Private $aLang As String[]
Static Private $aProgDir As String[]
Static Private $cProgCache As New Collection
Static Private $cMimeCache As New Collection
Static Private $cIconCache As New Collection

Property Read Path As String
Property Name As String
Property GenericName As String
Property Comment As String
Property Read ProgramName As String
Property Exec As String
Property Terminal As Boolean
Property Hidden As Boolean
Property NoDisplay As Boolean
Property Icon As String
Property MimeTypes As String[]
Property Categories As String[]
Property WorkingDir As String
Property Actions As String[]
Property Read AlternativeActions As Collection

'' An array of the files altenative actions.<p>
'' This property modifies the \[Desktop Action \<Key\>\] definitions and has no effect on the [/comp/gb.desktop/desktopfile/actions](Actions) property
Property Read DesktopActions As _DesktopFile_Actions
Private $hDesktopActions As _DesktopFile_Actions

'' A hidden pointer to the raw data that this class uses as a Collection. (use at your own risk)
Property Read _Data As Collection

Private Const IGNORE_LINE As String = "$@$_"

Private $cSettings As Collection

Private $sPath As String

Static Public Sub _init()

  Dim sLang As String
  Dim iPos As Integer
  Dim sModifier As String
  Dim sCountry As String

  $aLang = New String[]

  sLang = System.Language
  iPos = RInStr(sLang, ".")
  If iPos Then sLang = Left(sLang, iPos - 1)

  iPos = RInStr(sLang, "@")
  If iPos Then
    sModifier = Mid(sLang, iPos + 1)
    sLang = Left(sLang, iPos - 1)
  Endif

  iPos = RInStr(sLang, "_")
  If iPos Then
    sCountry = Mid$(sLang, iPos + 1)
    sLang = Left$(sLang, iPos - 1)
  Endif

  If sCountry And If sModifier Then $aLang.Add(sLang & "_" & sCountry & "@" & sModifier)
  If sCountry Then $aLang.Add(sLang & "_" & sCountry)
  If sModifier Then $aLang.Add(sLang & "@" & sModifier)
  $aLang.Add(sLang)

End

Public Sub _compare(hDesktopFile As DesktopFile) As Integer

  Return String.Comp(Name_Read(), hDesktopFile.Name)

End

Public Sub _new(Path As String)

  $sPath = Path
  $cSettings = LoadIniFile(Path)
  $hDesktopActions = New _DesktopFile_Actions As "DeskTopFileActions"

End

Static Public Sub _exit()

  $cProgCache = Null
  $cMimeCache = Null
  $cIconCache = Null

End

Static Public Sub _get(Program As String) As DesktopFile

  Dim hProg As DesktopFile
  Dim sDir As String

  If Program Not Ends ".desktop" Then Program &= ".desktop"

  hProg = $cProgCache[Program]

  If Not hProg Then

    For Each sDir In GetDesktopFileDirectories()
      If Exist(sDir &/ Program) Then
        hProg = New DesktopFile(sDir &/ Program)
        $cProgCache[Program] = hProg
        Break
      Endif
    Next

  Endif

  Return hProg

End

Private Function Name_Read() As String

  If Not CheckSettingExists(GetKey("Name")) Then Return ""
  Return $cSettings["[Desktop Entry]"][GetKey("Name")]

End

Private Sub GetSlot(sSlot As String) As Collection

  Dim cSlot As Collection
  
  cSlot = $cSettings[sSlot]
  If Not cSlot Then
    cSlot = New Collection
    $cSettings[sSlot] = cSlot
  Endif
  Return cSlot
  
End

Private Sub Name_Write(Value As String)

  GetSlot("[Desktop Entry]")[GetKey("Name")] = Value

End

Private Function Exec_Read() As String

  If Not CheckSettingExists("Exec") Then Return ""
  Return $cSettings["[Desktop Entry]"]["Exec"]

End

Private Sub Exec_Write(Value As String)

  GetSlot("[Desktop Entry]")["Exec"] = Value

End

Private Function Terminal_Read() As Boolean

  If Not CheckSettingExists("Terminal") Then Return False
  Return $cSettings["[Desktop Entry]"]["Terminal"] == "true"

End

Private Sub Terminal_Write(Value As Boolean)

  GetSlot("[Desktop Entry]")["Terminal"] = LCase(CStr(Value))

End

Private Function Hidden_Read() As Boolean

  If Not CheckSettingExists("Hidden") Then Return False
  Return $cSettings["[Desktop Entry]"]["Hidden"] == "true"

End

Private Sub Hidden_Write(Value As Boolean)

  GetSlot("[Desktop Entry]")["Hidden"] = LCase(CStr(Value))

End

Private Function NoDisplay_Read() As Boolean

  If Not CheckSettingExists("NoDisplay") Then Return False
  Return $cSettings["[Desktop Entry]"]["NoDisplay"] == "true"

End

Private Sub NoDisplay_Write(Value As Boolean)

  GetSlot("[Desktop Entry]")["NoDisplay"] = LCase(CStr(Value))

End

Private Function MimeTypes_Read() As String[]

  If Not CheckSettingExists("MimeType") Then Return [""]
  Return Split($cSettings["[Desktop Entry]"]["MimeType"], ";", "\\;", True)

End

Private Sub MimeTypes_Write(Value As String[])

  If Not Value Then value = [" "]
  GetSlot("[Desktop Entry]")["MimeType"] = Value.Join(";")

End

Private Function Categories_Read() As String[]

  If Not CheckSettingExists("Categories") Then Return [""]
  Return Split($cSettings["[Desktop Entry]"]["Categories"], ";", "\\;", True)

End

Private Sub Categories_Write(Value As String[])

  If Not Value Then value = [" "]
  GetSlot("[Desktop Entry]")["Categories"] = Value.Join(";")

End

Private Function GenericName_Read() As String

  If Not CheckSettingExists(GetKey("GenericName")) Then Return ""
  Return $cSettings["[Desktop Entry]"][GetKey("GenericName")]

End

Private Sub GenericName_Write(Value As String)

  GetSlot("[Desktop Entry]")[GetKey("GenericName")] = Value

End

Private Function Comment_Read() As String

  If Not CheckSettingExists(GetKey("Comment")) Then Return ""
  Return $cSettings["[Desktop Entry]"][GetKey("Comment")]

End

Private Sub Comment_Write(Value As String)

  GetSlot("[Desktop Entry]")[GetKey("Comment")] = Value

End

Private Function WorkingDir_Read() As String

  If Not CheckSettingExists("Path") Then Return ""
  Return $cSettings["[Desktop Entry]"]["Path"]

End

Private Sub WorkingDir_Write(Value As String)

  GetSlot("[Desktop Entry]")["Path"] = Value

End

Private Function Actions_Read() As String[]

  If Not CheckSettingExists("Actions") Then Return [""]
  Return Split($cSettings["[Desktop Entry]"]["Actions"], ";", "\\;", True)

End

Private Sub Actions_Write(Value As String[])

  If Not Value Then value = [" "]
  GetSlot("[Desktop Entry]")["Actions"] = Value.Join(";")

End

Private Function AlternativeActions_Read() As Collection

  Dim result As New Collection
  Dim sAction As String

  For Each sAction In Actions_Read()
    If sAction <> "" Then result.Add(Get_Actions_Exec(sAction), Get_Actions_Name(sAction))
  Next
  Return result

End

Public Function Get_Actions_Name(sAction As String) As String

  If Not $cSettings.Exist("[Desktop Action " & sAction & "]") Then Return ""
  If Not $cSettings["[Desktop Action " & sAction & "]"].Exist(GetKey("Name")) Then Return ""
  Return $cSettings["[Desktop Action " & sAction & "]"]["Name"]

End

Public Function Get_Actions_Exec(sAction As String) As String

  If Not $cSettings.Exist("[Desktop Action " & sAction & "]") Then Return ""
  If Not $cSettings["[Desktop Action " & sAction & "]"].Exist("Exec") Then Return ""
  Return $cSettings["[Desktop Action " & sAction & "]"]["Exec"]

End

Private Function Icon_Read() As String

  If Not CheckSettingExists("Icon") Then Return ""
  Return $cSettings["[Desktop Entry]"]["Icon"]

End

Private Sub Icon_Write(Value As String)

  GetSlot("[Desktop Entry]")["Icon"] = Value

End

Public Sub GetIcon(Optional Size As Integer = 16) As Image

  Dim sPath As String
  Dim hImage As Image
  Dim sKey As String
  Dim sIcon As String
  Dim sDir As String

  sKey = CStr(Size) & ":" & $sPath
  hImage = $cIconCache[sKey]
  If hImage Then Return hImage
  
  sIcon = Icon_Read()
  If sIcon Then
  
    If Not hImage Then
      sPath = Subst("/usr/share/icons/hicolor/&1x&1/apps/&2.png", CStr(Size), sIcon)
      If Exist(sPath) Then Try hImage = Image.Load(sPath)
    Endif
    If Not hImage Then
      sPath = Subst("/usr/share/pixmaps/&1.png", sIcon)
      If Exist(sPath) Then Try hImage = Image.Load(sPath).Stretch(Size, Size)
    Endif
    If Not hImage Then
      If sIcon Begins "/" Then
        Try hImage = Image.Load(sIcon).Stretch(Size, Size)
      Else If sIcon Begins "./" Then
        Try hImage = Image.Load(File.Dir($sPath) &/ Mid$(sIcon, 3)).Stretch(Size, Size)
      Else
        For Each sDir In [".apps", ".places", ".devices"]
          Try hImage = Stock[CStr(Size) &/ sDir &/ sIcon].Image
          If hImage Then Break
        Next
      Endif
    Endif
    
  Endif
    
  If Not hImage Then 
    If Exec_Read() Then
      hImage = Stock[CStr(Size) &/ "program"].Image
    Else If File.Name($sPath) = ".directory" Then
      hImage = Stock[CStr(Size) &/ "directory"].Image
    Else
      hImage = Stock[CStr(Size) &/ "file"].Image
    Endif
  Endif

  $cIconCache[sKey] = hImage
  Return hImage

End

Public Sub Save(Optional Path As String = $sPath)

  WriteIniFile($cSettings, Path)

End

Private Function GetKey(KeyBase As String, Optional sLang As String) As String

  Dim i As Integer

  If Not sLang Then 
    For i = 0 To $aLang.Max
      If CheckSettingExists(KeyBase & "[" & $aLang[i] & "]") Then
        sLang = "[" & $aLang[i] & "]"
        Break
      Endif
    Next
  Else
    sLang = "[" & sLang & "]"
    If Not CheckSettingExists(KeyBase & sLang) Then
      sLang = ""
    Endif
  Endif
  Return KeyBase & sLang

End

Public Function Exist() As Boolean

  Return FindExecutable(Exec_Read())

End

Static Public Function FindExecutable(name As String) As String

  Dim path As String
  Dim iPos As Integer

  name = ReplaceArgs(name, "")
  ipos = InStr(name, " ")
  If iPos > 0 Then name = Mid(name, 1, iPos - 1)
  If Exist(name) Then Return File.Dir(name)
  For Each path In Split(Application.Env["PATH"], ":", "", True)
    If Not Exist(path) Then Continue
    If Dir(path).Exist(name) Then Return path
  Next
  Return ""

End

Static Private Function ReplaceArgs(sExec As String, sArgs As String) As String

  sExec = Replace(sExec, "%U", sArgs)
  sExec = Replace(sExec, "%u", sArgs)
  sExec = Replace(sExec, "%F", sArgs)
  sExec = Replace(sExec, "%f", sArgs)
  '' TODO: implement %i, %c & %k
  sExec = Replace(sExec, "%i", "")
  sExec = Replace(sExec, "%c", "")
  sExec = Replace(sExec, "%k", "")
  Return Trim(sExec)

End

Private Function Path_Read() As String

  Return $sPath

End

Private Function ProgramName_Read() As String

  Dim sExec As String = Exec_Read()
  Dim iPos As Integer

  iPos = InStr(sExec, " ")
  If iPos = 0 Then
    Return sExec
  Else
    Return Left$(sExec, iPos - 1)
  Endif

End

Static Private Sub GetDesktopFileDirectories() As String[]

  Dim sDir As String
  Dim sRoot As String

  If Not $aProgDir Then

    $aProgDir = New String[]
    For Each sRoot In Main.GetDataDir()
      sRoot &/= "applications"
      $aProgDir.Add(sRoot)
      For Each sDir In RDir(sRoot, "*", gb.Directory)
        $aProgDir.Add(sRoot &/ sDir)
      Next
    Next

  Endif

  Return $aProgDir

End

Static Private Sub FindInMimeInfoCache(sPath As String, sMime As String) As String

  Dim sData As String = "\n" & File.Load(sPath) & "\n"
  Dim iPos As Integer
  Dim iPos2 As Integer

  iPos = InStr(sData, "\n" & sMime & "=")
  If iPos = 0 Then Return

  iPos2 = InStr(sData, "\n", iPos + 1)
  iPos = InStr(sData, "=", iPos + 1)
  Return Mid$(sData, iPos + 1, iPos2 - iPos - 1)

Catch
  '     Main.ErrorLogger(Error.Text, Logger.Error)

End

Static Public Sub FindMime(MimeType As String) As DesktopFile[]

  Error "gb.desktop: warning: FindMime() is deprecated, use FromMime() instead."
  Return FromMime(MimeType)

End

Static Public Sub FromMime(MimeType As String) As DesktopFile[]

  Dim hProgList As DesktopFile[]
  Dim hProg As DesktopFile
  Dim aMime As String[]
  Dim sMime As String
  Dim sFound As String
  Dim aPath As String[]
  Dim sPath As String
  Dim aList As String[]
  Dim sList As String

  hProgList = $cMimeCache[MimeType]

  If Not hProgList Then

    hProgList = New DesktopFile[]
    aMime = [MimeType]
    If MimeType Begins "text/" And If MimeType <> "text/plain" Then aMime.Add("text/plain")

    aPath = Main.MakeSearchPath("mimeapps.list", ["$XDG_CONFIG_HOME/$desktop-mimeapps.list", "$XDG_CONFIG_HOME/mimeapps.list", "$XDG_CONFIG_DIRS/$desktop-mimeapps.list", "$XDG_CONFIG_DIRS/mimeapps.list", "$XDG_DATA_DIRS/applications/$desktop-mimeapps.list", "$XDG_DATA_DIRS/applications/mimeapps.list"])
      
    aPath.Insert(Main.MakeSearchPath("mimeinfo.cache", ["$XDG_CONFIG_HOME/mimeinfo.cache", "$XDG_CONFIG_DIRS/mimeinfo.cache", "$XDG_DATA_DIRS/applications/mimeinfo.cache"]))
      
    For Each sMime In aMime

      aList = New String[]

      'Print "["; sMime; "]"
      
      For Each sPath In aPath
        sFound = FindInMimeInfoCache(sPath, sMime)
        If sFound Then aList.Insert(Split(sFound, ";"))
      Next

      'Print "--> "; sList

      For Each sList In aList
        If Not sList Then Continue
        hProg = DesktopFile[sList]
        If Not hProg Then hProg = DesktopFile[Replace(sList, "-", "/")]
        If Not hProg Then Continue
        If hProg.Terminal Then Continue
        If hProgList.Exist(hProg) Then Continue
        hProgList.Add(hProg)
      Next

    Next

    $cMimeCache[sMime] = hProgList

  Endif

  Return hProgList '.Sort()

End

Public Function Run(sArgs As String, Optional RunAsRoot As Boolean = False) As Process

  Dim sExec As String = Exec_Read()

  sExec = Replace(sExec, "%c", Name_Read())
  sExec = Replace(sExec, "%k", Name_Read())
  Return RunExec(sExec, sArgs, RunAsRoot)

End

Static Public Function RunExec(sExec As String, sArgs As String, Optional RunAsRoot As Boolean = False) As Process

  Dim cmd As String
  Dim aExec As String[]
  Dim I As Integer
  Dim hProcess As Process

  'keep quoted words together
  sExec = Replace(sExec, Chr(34), "'")
  aExec = Split(sExec, " ", "'", True, False)

  While I < aExec.Count
    sExec = ReplaceArgs(aExec[I], sArgs)
    If sExec Then
      aExec[I] = sExec
      Inc I
    Else
      aExec.Remove(I)
    Endif
  Wend

  If sArgs Then
    If Not aExec.Exist(sArgs) Then aExec.Add(sArgs)
  Endif
  If RunAsRoot Then
    Select Case UCase(Application.Theme)

      Case "KDE", "KDE4", "KDE5"
        cmd = "kdesudo"
        ' newer SuSE have no kdesudo, but a kdesu
        If Not System.Exist(sExec) Then cmd = "kdesu"
      Case "LXDE"
        cmd = "lxde-sudo"
      Case Else
        cmd = "gksudo"
        ' gksu with an argument raise a gksudo
        If Not System.Exist(cmd) Then cmd = "gksu"

    End Select

    If Not System.Exist(cmd) Then Error.Raise("No graphical sudo program found")
    'gksudo etc interprets any options provided by Exec as its own options
    'so we recombine the sudoed command and its options into one string
    sExec = aExec.Join(" ")
    aExec = [cmd]
    aExec.Add(sExec)
  Endif
  hProcess = Exec aExec
  Return hProcess

End

Static Private Function LoadIniFile(sPath As String) As Collection

  Dim sLine As String
  Dim iPos As Integer
  Dim sKey As String
  Dim sValue As String
  Dim cValue As Collection
  Dim cArray As Collection
  Dim sGroupKey As String
  Dim iIgnoreCount As Integer = 0
  Dim hFile As File

  cArray = New Collection
  If Not Exist(sPath) Then Return cArray

  hFile = Open sPath
  
  'aLine = Split(File.Load(Path), "\n", "", True)
  'If aLine.count = 0 Then Return cArray
  'For I = 0 To aLine.Max
  
  For Each sLine In hFile.Lines
    
    sLine = FixUpLine(sLine, True)
    
    If sLine Begins "[" And If sLine Ends "]" Then
      If cValue And If cValue.Count Then
        cArray[sGroupKey] = cValue
      Endif
      cValue = New Collection
      sGroupKey = sLine
      Continue
    Endif

    iPos = InStr(sLine, "=")

    If sLine Begins "#" Or If sLine Begins "[" Or If iPos < 2 Then
      'This catches comments,blank lines and invalid lines without a key
      Inc iIgnoreCount
      sKey = IGNORE_LINE & CStr(iIgnoreCount)
      sValue = sLine
    Else
      sKey = Trim(Left(sLine, iPos - 1))
      sValue = Trim(Mid$(sLine, iPos + 1))
    Endif

    If IsNull(cValue) Then
      cArray[sKey] = sValue
    Else
      cValue[sKey] = sValue
    Endif

  Next
  
  Close hFile

  cArray[sGroupKey] = cValue

  Return cArray

End

Static Private Sub WriteIniFile(cSettings As Collection, sPath As String)

  Dim hFile As File
  Dim item As Variant
  Dim item2 As String
  Dim sKey As String
  Dim cGroup As Collection

  hFile = Open sPath For Create
  
  For Each item In cSettings
    sKey = cSettings.Key
    If sKey Begins "[" Then
      Print #hFile, sKey
      cGroup = item
      For Each item2 In cGroup
        If cGroup.Key Begins IGNORE_LINE Then
          Print #hFile, FixUpLine(item2, False)
        Else
          Print #hFile, cGroup.Key & "=" & FixUpLine(item2, False)
        Endif
      Next
      Print #hFile
    Else
      If sKey Begins IGNORE_LINE Then
        Print #hFile, FixUpLine(item, False)
      Else
        Print #hFile, sKey & "=" & FixUpLine(item, False)
      Endif
    Endif
  Next
  
  Close #hFile

End

Private Function CheckSettingExists(sSetting As String) As Boolean

  If IsNull($cSettings) Then Return False
  If Not $cSettings.Exist("[Desktop Entry]") Then Return False
  Return $cSettings["[Desktop Entry]"].Exist(sSetting)

End

Static Private Function FixUpLine(sLine As String, bIn As Boolean) As String

  Dim sResult As String = Trim(sLine)

  If bIn
    sResult = Replace(sResult, "\\s", " ")
    sResult = Replace(sResult, "\\n", "\n")
    sResult = Replace(sResult, "\\t", "\t")
    sResult = Replace(sResult, "\\r", "\r")
    sResult = Replace(sResult, "\\\\", "\\")
  Else
    'leave tabs and spaces alone
    sResult = Replace(sResult, "\n", "\\n")
    sResult = Replace(sResult, "\r", "\\r")
    sResult = Replace(sResult, "\\", "\\\\")
  Endif
  Return sResult

End

Static Public Sub _Refresh()
  
  $cProgCache.Clear
  $cMimeCache.Clear
  $cIconCache.Clear
  
End

Static Public Function _Langs() As String[]
  
  Return $aLang
  
End


Private Function _Data_Read() As Collection

  Return $cSettings

End

Private Function DesktopActions_Read() As _DesktopFile_Actions

  Return $hDesktopActions

End

