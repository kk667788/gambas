#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.form 3.15.90\n"
"POT-Creation-Date: 2024-04-04 21:28 UTC\n"
"PO-Revision-Date: 2021-02-17 15:42 UTC\n"
"Last-Translator: Benoît Minisini <g4mba5@gmail.com>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "More controls for graphical components"
msgstr "Další ovládací prvky pro grafické komponenty"

#: CBookmarkList.class:51
msgid "Home"
msgstr "Domů"

#: CBookmarkList.class:53
msgid "Desktop"
msgstr "Pracovní plocha"

#: CBookmarkList.class:55
msgid "System"
msgstr "Systém"

#: ColorPalette.class:185
msgid "Last colors"
msgstr ""

#: ColorPalette.class:317
msgid "Remove color"
msgstr ""

#: ColorPalette.class:321
msgid "Remove all colors"
msgstr ""

#: ColorPalette.class:325
msgid "Sort colors"
msgstr ""

#: DirView.class:596
msgid "Cannot rename directory."
msgstr "Nelze přejmenovat adresář."

#: DirView.class:626
msgid "New folder"
msgstr "Nová složka"

#: DirView.class:645
msgid "Cannot create directory."
msgstr "Nelze vytvořit adresář."

#: FBugFileView.form:32
msgid "Toggle Fileview Detailed View"
msgstr ""

#: FBugFileView.form:37
msgid "Toggle Filechooser Detailed View"
msgstr ""

#: FCalendar.form:48
msgid "Today"
msgstr "Dnes"

#: FCalendar.form:54
msgid "Previous month"
msgstr "Předchozí měsíc"

#: FCalendar.form:60
msgid "Next month"
msgstr "Následující měsíc"

#: FCalendar.form:147
msgid "Apply"
msgstr "Aplikovat"

#: FColorChooser.form:81
msgid "Follow color grid"
msgstr "Následuj barvu mřížky"

#: FDirChooser.class:441
msgid "Directory not found."
msgstr ""

#: FDirChooser.class:550
msgid "All files (*)"
msgstr "Všechny soubory (*)"

#: FDirChooser.class:911
msgid "Overwrite"
msgstr "Přepsat"

#: FDirChooser.class:911
msgid ""
"This file already exists.\n"
"\n"
"Do you want to overwrite it?"
msgstr ""
"Tento soubor již existuje.\n"
"\n"
"Chcete jej přepsat?"

#: FDirChooser.class:1029
msgid "&Bookmark current directory"
msgstr "Přidat adresář do &záložek"

#: FDirChooser.class:1037
msgid "&Edit bookmarks..."
msgstr "Upravit &záložky..."

#: FDirChooser.class:1081
msgid "Preview big files"
msgstr ""

#: FDirChooser.class:1090
#, fuzzy
msgid "Show hidden files"
msgstr "Zobrazit &skryté soubory"

#: FDirChooser.class:1098
#, fuzzy
msgid "Rename"
msgstr "Pře&jmenovat"

#: FDirChooser.class:1103
msgid "Copy"
msgstr "Kopírovat"

#: FDirChooser.class:1108
#, fuzzy
msgid "Delete"
msgstr "S&mazat"

#: FDirChooser.class:1120
msgid "&Uncompress file"
msgstr "&Rozbalit soubor"

#: FDirChooser.class:1125
msgid "&Create directory"
msgstr "Vy&tvořit adresář"

#: FDirChooser.class:1130
msgid "Open in &file manager..."
msgstr "Otevřít v &souborevém manageru..."

#: FDirChooser.class:1135
msgid "&Refresh"
msgstr "&Obnovit"

#: FDirChooser.class:1143
msgid "&Properties"
msgstr "&Vlastnosti"

#: FDirChooser.class:1333
msgid "Overwrite all"
msgstr "Vše přepsat"

#: FDirChooser.class:1333
msgid "This file or directory already exists."
msgstr "Tento soubor nebo adresář již existuje."

#: FDirChooser.class:1354
msgid "Cannot list archive contents"
msgstr ""

#: FDirChooser.class:1394
msgid "Cannot uncompress file."
msgstr "Nelze rozbalit soubor."

#: FDirChooser.class:1394
msgid "Unknown archive."
msgstr "Neznámí archív."

#: FDirChooser.class:1465
msgid "Delete file"
msgstr "Smazat soubor"

#: FDirChooser.class:1466
msgid "&Delete"
msgstr "S&mazat"

#: FDirChooser.class:1466
msgid "Do you really want to delete that file?"
msgstr "Opravdu chcete smazat tento soubor?"

#: FDirChooser.class:1473
msgid "Unable to delete file."
msgstr "Soubor nelze odstranit."

#: FDirChooser.class:1483
msgid "Delete directory"
msgstr ""

#: FDirChooser.class:1484
msgid "Do you really want to delete that directory?"
msgstr ""

#: FDirChooser.class:1491
msgid "Unable to delete directory."
msgstr ""

#: FDirChooser.form:76
msgid "Parent directory"
msgstr "Rodičovský adresář"

#: FDirChooser.form:82
msgid "Root directory"
msgstr ""

#: FDirChooser.form:153
msgid "Icon view"
msgstr ""

#: FDirChooser.form:164
msgid "Compact view"
msgstr ""

#: FDirChooser.form:174
#, fuzzy
msgid "Preview view"
msgstr "Náhled"

#: FDirChooser.form:184
msgid "Detailed view"
msgstr ""

#: FDirChooser.form:197
msgid "File properties"
msgstr ""

#: FDirChooser.form:203
msgid "Show files"
msgstr "Zobrazit soubory"

#: FDirChooser.form:223
msgid "Bookmarks"
msgstr "Záložky"

# gb-ignore
#: FDirChooser.form:290 FInputBox.form:43 FWizard.class:76 Form1.form:41
msgid "OK"
msgstr ""

#: FDirChooser.form:296 FEditBookmark.class:119 FInputBox.form:49
#: FSidePanel.class:1144 FWizard.form:52 Form1.form:47
msgid "Cancel"
msgstr "Zrušit"

#: FDocumentView.form:58
msgid "Zoom :"
msgstr ""

#: FDocumentView.form:63
msgid "Show Shadow"
msgstr ""

#: FDocumentView.form:73 FTestTabPanel.form:72
msgid "Padding"
msgstr ""

#: FDocumentView.form:78
msgid "Spacing"
msgstr ""

#: FDocumentView.form:87
msgid "Scale Mode"
msgstr ""

#: FDocumentView.form:96
msgid "Goto :"
msgstr ""

#: FDocumentView.form:102
msgid "Column"
msgstr ""

#: FDocumentView.form:102
msgid "Fill"
msgstr ""

#: FDocumentView.form:102
msgid "Horizontal"
msgstr ""

#: FDocumentView.form:102
msgid "None"
msgstr ""

#: FDocumentView.form:102
msgid "Row"
msgstr ""

#: FDocumentView.form:102
msgid "Vertical"
msgstr ""

#: FDocumentView.form:103 FMain.form:118
msgid "ComboBox1"
msgstr ""

#: FDocumentView.form:108 FMain.form:103 FTestBalloon.form:19
#: FTestCompletion.form:23 FTestMenuButton.form:150 FTestMessageView.form:27
#: FTestWizard.form:24
msgid "Button1"
msgstr ""

#: FDocumentView.form:117
msgid "Columns"
msgstr ""

#: FDocumentView.form:127
msgid "Center"
msgstr ""

#: FEditBookmark.class:23 FileView.class:197
msgid "Name"
msgstr "Název"

#: FEditBookmark.class:24
msgid "Path"
msgstr "Cesta"

#: FEditBookmark.class:119
msgid "Do you really want to remove this bookmark?"
msgstr "Opravdu chcete odstranit tuto záložku?"

#: FEditBookmark.form:15
msgid "Edit bookmarks"
msgstr "Upravit záložky"

#: FEditBookmark.form:34
msgid "Up"
msgstr "Nahoru"

#: FEditBookmark.form:40
msgid "Down"
msgstr "Dolů"

#: FEditBookmark.form:46 FListEditor.class:277
msgid "Remove"
msgstr "Odebrat"

#: FEditBookmark.form:57 FFileProperties.form:290
msgid "Close"
msgstr "Zavřít"

#: FFileProperties.class:120
msgid "Image"
msgstr "Obrázek"

#: FFileProperties.class:125
msgid "Audio"
msgstr ""

# gb-ignore
#: FFileProperties.class:129
msgid "Video"
msgstr ""

#: FFileProperties.class:189
msgid "&1 properties"
msgstr "&1 vlastnosti"

# gb-ignore
#: FFileProperties.class:220 Main.module:375
msgid "&1 B"
msgstr ""

#: FFileProperties.class:225
msgid "no file"
msgstr ""

#: FFileProperties.class:227
msgid "one file"
msgstr ""

#: FFileProperties.class:229
msgid "files"
msgstr ""

#: FFileProperties.class:233
msgid "no directory"
msgstr ""

#: FFileProperties.class:235
msgid "one directory"
msgstr ""

#: FFileProperties.class:237
msgid "directories"
msgstr ""

#: FFileProperties.form:56
msgid "General"
msgstr "Obecně"

#: FFileProperties.form:86
msgid "Type"
msgstr "Typ"

#: FFileProperties.form:99
msgid "Link"
msgstr ""

#: FFileProperties.form:113
msgid "Directory"
msgstr "Adresář"

#: FFileProperties.form:125 FileView.class:198
msgid "Size"
msgstr "Velikost"

#: FFileProperties.form:137 FileView.class:199
msgid "Last modified"
msgstr "Poslední změna"

#: FFileProperties.form:149
msgid "Last access"
msgstr ""

#: FFileProperties.form:161 FileView.class:200
msgid "Permissions"
msgstr "Oprávnění"

#: FFileProperties.form:174
msgid "Owner"
msgstr "Vlastník"

#: FFileProperties.form:186
msgid "Group"
msgstr "Skupina"

#: FFileProperties.form:196
msgid "Preview"
msgstr "Náhled"

#: FFileProperties.form:261
msgid "Errors"
msgstr "Chyby"

#: FFontChooser.class:374
msgid "How quickly daft jumping zebras vex"
msgstr "Příliš žluťoučký kůň úpěl ďábelské ódy"

#: FFontChooser.form:73
msgid "Building cache"
msgstr ""

#: FFontChooser.form:80
#, fuzzy
msgid "Refresh cache"
msgstr "&Obnovit"

#: FFontChooser.form:86
#, fuzzy
msgid "Show font preview"
msgstr "Zobrazit &náhled obrázku"

#: FFontChooser.form:110
msgid "Bold"
msgstr "Tučné"

#: FFontChooser.form:117
msgid "Italic"
msgstr "Kurzíva"

#: FFontChooser.form:124
msgid "Underline"
msgstr "Podtržené"

#: FFontChooser.form:131
msgid "Strikeout"
msgstr "Přeškrknuté"

#: FFontChooser.form:140
msgid "Relative"
msgstr "Relativní"

#: FIconPanel.form:18
msgid "Tab 1"
msgstr ""

#: FIconPanel.form:22 FTestTabPanel.form:33
msgid "Toto"
msgstr ""

#: FLCDLabel.form:15
msgid "12:34"
msgstr ""

#: FListEditor.class:276
msgid "Add"
msgstr "Přidat"

#: FListEditor.form:45
msgid "Add item"
msgstr ""

#: FListEditor.form:53
msgid "Remove item"
msgstr ""

#: FListEditor.form:61
msgid "Move item up"
msgstr ""

#: FListEditor.form:69
msgid "Move item down"
msgstr ""

#: FMain.class:25
#, fuzzy
msgid "PDF files"
msgstr "Zobrazit soubory"

#: FMain.class:25
msgid "Postscript files"
msgstr ""

#: FMain.form:31 FTestFileChooser.form:31 FTestMenuButton.form:40 FWiki.form:20
msgid "Menu2"
msgstr ""

#: FMain.form:35 FTestFileChooser.form:36 FTestMenuButton.form:44 FWiki.form:24
msgid "Menu3"
msgstr ""

#: FMain.form:84
msgid "http://gambas.sf.net"
msgstr ""

#: FMain.form:92
msgid "ComboBox2"
msgstr ""

#: FMain.form:98 FTestBalloon.form:13 FTestFileChooser.form:86
#: FTestSwitchButton.form:25
msgid "TextBox1"
msgstr ""

#: FMain.form:108 Form2.form:121
msgid "MenuButton1"
msgstr ""

#: FMain.form:117 FTestListEditor.form:11
msgid "Élément 1"
msgstr ""

#: FMain.form:117 FTestListEditor.form:11
msgid "Élément 2"
msgstr ""

#: FMain.form:117 FTestListEditor.form:11
msgid "Élément 3"
msgstr ""

#: FMain.form:117 FTestListEditor.form:11
msgid "Élément 4"
msgstr ""

#: FMessage.form:39
msgid "Do not display this message again"
msgstr "Již nezobrazovat"

#: FSidePanel.class:1123
msgid "Hidden"
msgstr "Skryté"

#: FSidePanel.class:1130
#, fuzzy
msgid "Transparent handle"
msgstr "Průhlednost"

#: FSidePanel.class:1138
msgid "Hide automatically"
msgstr ""

#: FSpinBar.form:24
msgid "Test"
msgstr ""

#: FTestBalloon.form:18
msgid "Ceci est une bulle d'aide"
msgstr ""

#: FTestColorChooser.form:20
msgid "Resizable"
msgstr ""

#: FTestCompletion.form:28 FTestMessageView.form:38
msgid "Button2"
msgstr ""

#: FTestDateChooser.form:33
msgid "Enable"
msgstr ""

#: FTestExpander.form:16
msgid "Expander"
msgstr ""

#: FTestFileBox.form:17
msgid "Open a file"
msgstr ""

#: FTestFileBox.form:18
msgid "File to open"
msgstr ""

#: FTestFileBox.form:24
#, fuzzy
msgid "Save file as"
msgstr "Zobrazit soubory"

#: FTestFileBox.form:25
msgid "File to save"
msgstr ""

#: FTestFileBox.form:30
msgid "Open image file"
msgstr ""

#: FTestFileBox.form:33
#, fuzzy
msgid "Image filters"
msgstr "Náhled obrázku"

#: FTestFileChooser.form:28 FTestMenuButton.form:36
msgid "Menu1"
msgstr ""

#: FTestFileChooser.form:41 FTestMenuButton.form:70
msgid "Menu7"
msgstr ""

#: FTestFileChooser.form:49 FTestMenuButton.form:53 FWiki.form:28
msgid "Menu4"
msgstr ""

#: FTestFileChooser.form:54 FTestMenuButton.form:57 FWiki.form:32
msgid "Menu5"
msgstr ""

#: FTestFileChooser.form:76
msgid "Balloon"
msgstr ""

#: FTestFileChooser.form:81 FTestSwitchButton.form:42
msgid "Label1"
msgstr ""

#: FTestFileView.form:22
msgid "Selection"
msgstr ""

#: FTestListEditor.form:11
msgid "Élément 5"
msgstr ""

#: FTestMenuButton.form:32
msgid "Project"
msgstr ""

#: FTestMenuButton.form:49
msgid "View"
msgstr ""

#: FTestMenuButton.form:61
msgid "Menu6"
msgstr ""

#: FTestMenuButton.form:66
msgid "Tools"
msgstr ""

#: FTestMenuButton.form:74
msgid "Menu8"
msgstr ""

#: FTestMenuButton.form:78
msgid "Menu9"
msgstr ""

#: FTestMenuButton.form:81
msgid "Menu10"
msgstr ""

#: FTestMenuButton.form:85
msgid "Menu11"
msgstr ""

#: FTestMenuButton.form:124
msgid "Menu button"
msgstr ""

#: FTestSwitchButton.form:49
msgid "Label2"
msgstr ""

#: FTestTabPanel.form:43
msgid "Text"
msgstr ""

#: FTestTabPanel.form:49
msgid "Tab 4"
msgstr ""

#: FTestTabPanel.form:51
msgid "Tab 5"
msgstr ""

#: FTestTabPanel.form:53
msgid "Tab 6"
msgstr ""

#: FTestTabPanel.form:62
msgid "Border"
msgstr ""

#: FTestTabPanel.form:67
msgid "Orientation"
msgstr ""

#: FTestTabPanel.form:77
msgid "TabBar"
msgstr ""

#: FTestToolPanel.form:17
msgid "Toolbar 1"
msgstr ""

#: FTestToolPanel.form:19
msgid "Toolbar 2"
msgstr ""

#: FTestToolPanel.form:21
msgid "Toolbar 3"
msgstr ""

#: FTestToolPanel.form:23
msgid "Toolbar 4"
msgstr ""

#: FTestValueBox.form:16
msgid "Hello world!"
msgstr ""

#: FTestWizard.form:20
msgid "Étape n°1"
msgstr ""

#: FTestWizard.form:27
msgid "Ceci est une longue étape"
msgstr ""

#: FTestWizard.form:33
msgid "Étape n°3"
msgstr ""

#: FTestWizard.form:35
msgid "Étape n°4"
msgstr ""

#: FWizard.class:88
msgid "&Next"
msgstr "&Další"

#: FWizard.form:58
msgid "Next"
msgstr "Další"

#: FWizard.form:64
msgid "Previous"
msgstr "Předchozí"

#: FileView.class:220
msgid "This folder is empty."
msgstr ""

#: FileView.class:1369
msgid "Cannot rename file."
msgstr "Nelze přejmonovat soubor."

#: Form2.form:126
msgid "ButtonBox2"
msgstr ""

#: Form3.form:25
msgid "Raise"
msgstr ""

#: Help.module:71
msgid "A file or directory name cannot be void."
msgstr "Název souboru nebo adresáře nemůže být neplatný."

#: Help.module:72
msgid "The '/' character is forbidden inside file or directory names."
msgstr "Uvnitř názvu souboru nebo adresáře je znak '/' zakázán."

# gb-ignore
#: Main.module:377
msgid "&1 KiB"
msgstr ""

# gb-ignore
#: Main.module:379
msgid "&1 MiB"
msgstr ""

# gb-ignore
#: Main.module:381
msgid "&1 GiB"
msgstr ""

#: Main.module:387
msgid "&1 KB"
msgstr ""

#: Main.module:389
msgid "&1 MB"
msgstr ""

#: Main.module:391
msgid "&1 GB"
msgstr ""

#: Wizard.class:86
msgid "Step #&1"
msgstr "Krok #&1"

#~ msgid "&Overwrite"
#~ msgstr "&Přepsat"

#~ msgid "Show &details"
#~ msgstr "Zobrazit &detaily"
