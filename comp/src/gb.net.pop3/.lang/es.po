#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.net.pop3 3.17.90\n"
"PO-Revision-Date: 2023-06-06 18:26 UTC\n"
"Last-Translator: Martin Belmonte <info@belmotek.net>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

# gb-ignore
#: .project:1
msgid "gb.net.pop3"
msgstr "-"

#: .project:2
msgid "Gambas implementation of the POP3 protocol."
msgstr "Implementación del protocolo POP3 en Gambas"

