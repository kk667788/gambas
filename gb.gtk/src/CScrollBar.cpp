/***************************************************************************

  CScrollBar.cpp

  (c) 2004-2006 - Daniel Campos Fernández <dcamposf@gmail.com>
  (c) Benoît Minisini <benoit.minisini@gambas-basic.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2, or (at your option)
  any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  MA 02110-1301, USA.

***************************************************************************/

#define __CSCROLLBAR_CPP

#include "main.h"
#include "gambas.h"
#include "widgets.h"

#include "CScrollBar.h"
#include "CContainer.h"
#include "CWidget.h"

DECLARE_EVENT(EVENT_Change);

void CB_slider_change(gSlider *sender)
{
	CB_GET_OBJECT(sender);
	GB.Raise(THIS, EVENT_Change, 0);
}

//---------------------------------------------------------------------------


BEGIN_METHOD(ScrollBar_new, GB_OBJECT parent)

	InitControl(new gScrollBar(CONTAINER(VARG(parent))),(CWIDGET*)THIS);
	
END_METHOD

BEGIN_PROPERTY(ScrollBar_Tracking)

	if (READ_PROPERTY) { GB.ReturnBoolean(SCROLLBAR->tracking()); return; }
	SCROLLBAR->setTracking(VPROP(GB_BOOLEAN));

END_PROPERTY

BEGIN_PROPERTY(ScrollBar_Value)

	if (READ_PROPERTY) { GB.ReturnInteger(SCROLLBAR->value()); return; }
	SCROLLBAR->setValue(VPROP(GB_INTEGER));

END_PROPERTY

BEGIN_PROPERTY(ScrollBar_MinValue)

	if (READ_PROPERTY) { GB.ReturnInteger(SCROLLBAR->min()); return; }
	SCROLLBAR->setMin(VPROP(GB_INTEGER));

END_PROPERTY

BEGIN_PROPERTY(ScrollBar_MaxValue)

	if (READ_PROPERTY) { GB.ReturnInteger(SCROLLBAR->max()); return; }
	SCROLLBAR->setMax(VPROP(GB_INTEGER));

END_PROPERTY

BEGIN_PROPERTY(ScrollBar_LineStep)

	if (READ_PROPERTY) { GB.ReturnInteger(SCROLLBAR->step()); return; }
	SCROLLBAR->setStep(VPROP(GB_INTEGER));

END_PROPERTY

BEGIN_PROPERTY(ScrollBar_PageStep)

	if (READ_PROPERTY) { GB.ReturnInteger(SCROLLBAR->pageStep()); return; }
	SCROLLBAR->setPageStep(VPROP(GB_INTEGER));

END_PROPERTY

BEGIN_PROPERTY(ScrollBar_DefaultSize)

	GB.ReturnInteger(SCROLLBAR->getDefaultSize());

END_PROPERTY

BEGIN_PROPERTY(ScrollBar_Orientation)

	if (READ_PROPERTY)
		GB.ReturnInteger(SCROLLBAR->orientation());
	else
		SCROLLBAR->setOrientation(VPROP(GB_INTEGER));

END_PROPERTY

//-------------------------------------------------------------------------

GB_DESC ScrollBarDesc[] =
{
  GB_DECLARE("ScrollBar", sizeof(CSCROLLBAR)), GB_INHERITS("Control"),

  GB_METHOD("_new", 0, ScrollBar_new, "(Parent)Container;"),

  GB_PROPERTY_READ("DefaultSize", "i", ScrollBar_DefaultSize),

  GB_PROPERTY("Tracking", "b", ScrollBar_Tracking),
  GB_PROPERTY("Value", "i", ScrollBar_Value),
  GB_PROPERTY("MinValue", "i", ScrollBar_MinValue),
  GB_PROPERTY("MaxValue", "i", ScrollBar_MaxValue),
  GB_PROPERTY("Step", "i", ScrollBar_LineStep),
  GB_PROPERTY("PageStep", "i", ScrollBar_PageStep),
  GB_PROPERTY("Orientation", "i", ScrollBar_Orientation),

  GB_EVENT("Change", 0, 0, &EVENT_Change),

  SCROLLBAR_DESCRIPTION,

	GB_CONSTANT("Auto", "i", ORIENTATION_AUTO),
	GB_CONSTANT("Horizontal", "i", ORIENTATION_HORIZONTAL),
	GB_CONSTANT("Vertical", "i", ORIENTATION_VERTICAL),

  GB_END_DECLARE
};


